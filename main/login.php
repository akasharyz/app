<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/final/assets/constants.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/final/assets/dbfunctions.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/final/assets/validate.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/final/assets/helpers.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/final/error.php');


class login{


    function __construct()
    {
        global $helper,$validate,$error;
        //$helper = new helpers();
        $error = new Error();
        $validate = new validate();
        $this->login();

    }

    function __destruct()
    {

    }

    function login()
    {
        global $validate,$helper,$error;
        $intialToken = $_POST['initialToken'];
        if($validate->validateIntialToken($intialToken))
        {
            $helper->response(TRUE,OK,"Token verified",NULL);
        }
        else {
                $helper->response(FALSE,OK,"Token is incorrect",NULL);
        }

    }
}

$login = new login();
