<?php
require 'assets/helpers.php';
require 'assets/constants.php';

class error{


    function __construct()
    {
       global $helper;
       $helper = new helpers();
    }

    function linkError ()
    {
        global $helper;
        $helper->response(FALSE,linknotfound,'Link Not Found, Please enter Valid Link',NULL);
    }

}